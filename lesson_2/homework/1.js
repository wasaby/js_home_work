
  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */

  var buttons = document.querySelectorAll('header .showButton');
  var tabContainer = document.getElementById('tabContainer');
  var tabs = tabContainer.children;

  function hideAllTabs() {
      Array.from(tabs).forEach(function (tab) {
          tab.classList.remove('active');
      })
  }

  function showTab(tabIndex) {
      Array.from(tabs).forEach(function (tab) {
          if(tab.dataset.tab == tabIndex){
              tab.classList.add('active');
          }
      })
  }

  buttons.forEach(function (el) {
      el.onclick = function () {
          hideAllTabs();
          showTab(this.dataset.tab);
      }
  });

